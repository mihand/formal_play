(* Peano axiomatisation of natural numbers
   Some essential properties are implicit in Coq's logic:
      The fact that S is injective
      The fact that S 0 != 0
*)

Module m_peano_mul_n.
Load peano_add_n.

Fixpoint mul (a:nat) (b:nat) : nat :=
match a with
| o => o
| S p => (add (mul p b) b)
end.

Definition one := S o.

Notation "a * b" := (mul a b).

Theorem one_neutr_left:
forall b:nat, one * b = b.
Proof.
intros b.
unfold one.
unfold mul.
unfold add.
trivial.
(* simpl works here. But I like to see the explicit steps.
   after all it seems that unfold is the way to 'apply' a definition.
   Revisit the cases above that requied introducing a lemma or assert.
*)
Qed.

Theorem one_neutr_right:
forall b:nat, b * one = b.
Proof.
induction b.
- unfold mul.
  trivial.
- simpl mul.
  rewrite IHb.
  unfold one.
  rewrite <- s_commutes_with_add.
  rewrite _o_commutes_right.
  trivial.
Qed.

Theorem o_mul_right:
forall b:nat, b * o = o.
Proof.
intros b.
induction b.
- unfold mul. trivial.
- simpl mul.
  rewrite IHb.
  unfold add.
  trivial.
Qed.

Lemma lem_m1:
forall a b:nat, (S a) * b + a = (a * b + b) + a.
Proof.
intros a b.
simpl mul.
trivial.
Qed.

(*
Lemma _apply_commute_second_term:
forall x y z:nat, add x (add y z) = add x (add z y).
Proof.
intros x y z.
(* amazingly difficult to apply a damn rewrite to a subterm 
   This seems to work but why 
   pattern (add y z).
   rewrite add_commutes .
   trivial.
*)
set (g := (add y z)) .
rewrite add_commutes in g.
Qed.
*)

Lemma lem_m2:
forall a b:nat, a * (S b) + b = ((a * b) + b) + a.
Proof.
intros a b.
induction a.
- simpl mul. 
  simpl add. 
  rewrite _o_commutes_right. trivial.
- simpl mul. 
  rewrite add_assoc. 
  (*replace (add (S b) b) with (add b (S b)).*)
  pattern ((S b) + b). (* jezuz this *)
  rewrite add_commutes.
  rewrite <- add_assoc. 
  rewrite IHa.
  set ( bla := ((a * b) + b) ).
  rewrite add_assoc.
  rewrite add_assoc.
  rewrite <- s_commutes_with_add. 
  pattern ((S a) + b).
  rewrite add_commutes.
  trivial.
Qed.

Lemma lem_m: 
forall a b:nat, (S a) * b + a = a * (S b) + b.
Proof.
intros a b.
rewrite lem_m1.
rewrite lem_m2.
trivial.
Save.

Theorem add_elim_left:
forall a b c:nat, a + b = a + c -> b = c .
Proof.
intros a b c.
induction a.
- unfold add. trivial.
- intros G.
  simpl add in G.
  injection G.
  intros Gp.
  apply IHa in Gp.
  trivial.
Save.

Theorem add_elim_right:
forall a b c:nat, b + a = c + a -> b = c .
Proof.
intros a b c.
rewrite add_commutes.
pattern (c + a).
rewrite add_commutes.
apply add_elim_left.
Save.

Theorem mul_commutes:
forall a b:nat, a * b = b * a.
Proof.
intros a b.
induction a.
- rewrite o_mul_right.
  unfold mul. trivial.
- simpl mul. (* unfold does not work here. why again. *)
  rewrite IHa.
 (* is there no way to simply name the sides of the equality? 
  We apply the add elimination by giving the sides explicitly.
  This step is interesting because we apply a implication <- not a equivalency. *)
  apply (add_elim_right a (b * a + b) (b * (S a)) ).
  rewrite <- lem_m.
  simpl mul.
  rewrite add_assoc.
  (* the assoc rewrite has a direction. this is why the 
   second rewrite does not undo the first, it is no longer in the required form
   Unlike add_commutes which is symetric and will undo *)
  rewrite add_assoc. 
  pattern (a + b).
  rewrite add_commutes.
  trivial.
Qed.

(* Obs. This proof was not easy. After a few tried it was clear that we had to
 proove the lemma lem_m. A mul variant of the s_commutes_with_add lemma.
   It is interesting how much mul_commutes depends on almost all the properties
 of add proven this far. It requires add assoc commute and the elimination 
  a + x = b + x -> a = b. Elimination is like a poor version of a inverse element.
 So mul commute requires that add is almost an abelian group.
 Interesting
*)

(* let's prove some elimination results for mul 
  and the fact that 1 is the neutral element *)

Lemma only_zeros_add_to_zero:
forall x y:nat, x + y = o -> (x = o /\ y = o).
Proof.
intros x y H.
destruct x.
- unfold add in H. split.
  + trivial.
  + assumption.
- simpl add in H.
  (*inversion is nice to reduce constructors. Here it reduced to False *)
  inversion H. 
Save.

Lemma o_uniq_anihilator:
forall a b:nat, (S a) * b = o -> b = o.
Proof.
intros a b H.
simpl mul in H.
apply only_zeros_add_to_zero in H.
destruct H as [H1 H2]. (* inversion works too *)
assumption.
Save.

Theorem right_div_1:
forall a b:nat, a * (S b) = (S b) -> a = one.
Proof.
intros a b H.
destruct a.
- unfold mul in H.
  inversion H.
- simpl mul in H.
  apply add_elim in H.
  rewrite mul_commutes in H.
  apply o_uniq_anihilator in H.
  rewrite H.
  trivial.
Save.

(* forall a b:nat, q <> o, (mul a q) = (mul b q) -> a = b
   Write it with Sq instead so that we avoid a conditional.
   Note that this proof is a bit more sophisticated.
   We have too keep the forall quantifier in the induction steps.
   Lifting it in the itros will weaken the hypotesis and we will not be able to prove it.
*)
Theorem right_div:
forall q a b:nat, a * (S q) = b * (S q) -> a = b.
Proof.
intros q.
induction a.
- intros b H.
  simpl mul in H. 
  rewrite mul_commutes in H. 
  symmetry in H. 
  apply o_uniq_anihilator in H.
  symmetry in H.
  assumption.
- destruct b. 
   (* this is subtle. Look at IHa! If b would not have been forall b in IHa here, 
    it would have become S b in IHa ! because of the desctruct. 
    This would be a much weaker statement. *)
  + simpl (o * (S q)).
    rewrite mul_commutes.
    apply o_uniq_anihilator.
  + intros H. 
    simpl mul in H.
    apply add_elim_right in H.
    apply IHa in H. (* this is sublte *)
    rewrite H.
    trivial.
Save.

Lemma mul_rule2:
forall a b: nat,  (S a) * b = a * b + b.
Proof.
simpl mul. trivial.
Save.

Theorem mul_distributes_left:
forall a b c: nat, a * (b + c) = a * b + a * c.
Proof.
intros a b c.
induction a.
- unfold mul. unfold add. trivial.
- set (rhs := (S a) * b + (S a) * c).  
  simpl mul.
  rewrite IHa.
  (* jesuzz ! the term rewrite madness !! *)
  rewrite add_assoc with (a * b) (a * c) (b + c).
  rewrite add_commutes with b c.
  rewrite <- add_assoc with (a * c) c b.
  rewrite add_commutes with ((a * c) + c) b.
  rewrite <- add_assoc.
  (* end madness *)
  rewrite <- mul_rule2.
  rewrite <- mul_rule2.
  trivial.
Save.

Theorem mul_distributes_right:
forall a b c:nat, (a + b) * c = a*c + b*c.
Proof.
intros a b c.
rewrite mul_commutes.
rewrite mul_distributes_left.
rewrite mul_commutes with c a.
rewrite mul_commutes with c b.
trivial.
Save.

Theorem mul_assoc:
forall a b c:nat, a * (b * c) = (a * b) * c.
Proof.
intros a b c.
induction a.
- unfold mul. trivial.
- simpl mul.
  rewrite mul_distributes_right.
  rewrite IHa.
  trivial.
Save.

