(* Peano axiomatisation of natural numbers
   Some essential properties are implicit in Coq's logic:
      The fact that S is injective
      The fact that S 0 != 0
*)

Module m_peano_add.

Inductive nat: Type :=
| o : nat
| S : nat -> nat
.

Fixpoint add (a: nat) (b: nat) : nat :=
match a with
| o => b
| S p => S( (add p b))
end.

(* How to write the proof in the steps you would do on paper?
   (add (S o) b) -def add-> (S (add o b)) -def add-> (S b)
   (add o (S b)) -def add-> (S b)
   Below is as detailed i got.
   https://pjreddie.com/coq-tactics/#discriminate
   .simpl works of course.
*)
Lemma o_commutes_with_S :
forall b:nat, (add (S o) b) = (add o (S b)) .
Proof.
intro b.
unfold add.
trivial. 
Defined.

Example bla:
forall b:nat, (add (add o o) (S (S o))) = (S(S(o))).
Proof.
intro b.
unfold add. (* is there no way to rewrite a specific term ?? *)
trivial.
Qed.

(* introduced to use this specific rewrite. 
Why can't i directly use add's definition? 
*)
Lemma add_branch2:
forall a b:nat, (add (S a) b) = (S (add a b)).
Proof.
intros.
simpl.
trivial.
Qed.
 
Theorem s_commutes_with_add:
forall a:nat, forall b:nat, (add (S a) b) = (add a (S b)).
Proof.
intros a b.
induction a.
- apply o_commutes_with_S.
- rewrite add_branch2. 
  rewrite IHa.
  rewrite add_branch2. 
  trivial.
Save.

Lemma _o_commutes_right:
forall b: nat, (add b o) = b.
Proof.
intros b.
induction b.
- simpl. trivial.
- rewrite add_branch2.
  rewrite IHb.
  trivial.
Save.

Theorem o_commutes:
forall b: nat, (add o b) = (add b o).
Proof.
intros b.
simpl.
rewrite _o_commutes_right.
trivial.
Save.

Theorem add_commutes : 
forall a b:nat, (add a b) = (add b a).
Proof.
intros a b.
induction a.
- apply o_commutes.
- rewrite add_branch2.
  rewrite IHa.
  rewrite <- add_branch2.
  rewrite <- s_commutes_with_add.
  trivial.
Save.

Theorem add_elim:
forall a b:nat, (add a b) = b -> a = o .
Proof.
intros a b H.
induction b.
- rewrite _o_commutes_right in H.
  assumption.
- rewrite <- s_commutes_with_add in H.
  rewrite add_branch2 in H.
  injection H. (* Sa = Sb -> a = b. It will add the a=b fact as an antecendent of the goal *)
  intros Hp. (* push the antecedent as a hypotesis *)
  apply IHb in Hp. (* apply k and k->y proves y *)
  assumption. (* goal is Hp assumption now yey! *)
Qed.
(* observe that H was pushed as a hypotesis because we have to prove H -> a=o
   So it is a hypotesis in the induction SN step.
   However the N step of the induction must remain a -> the IHb hypothesis
*)

(* Interesting this would work as well, keeping the -> antecedent *)
Theorem add_elim_alternative:
forall a b:nat, (add a b) = b -> a = o .
Proof.
intros a b.
induction b.
- rewrite _o_commutes_right. trivial. 
- rewrite <- s_commutes_with_add.
  rewrite add_branch2.
  (* no idea how to apply injection in the antecedent so we push the 
     antecedent as a hypothesis
  *)
  intros H.
  injection H.
  assumption.
Qed.
 
Theorem sn_never_n : 
forall n:nat, S n = n -> False.
Proof.
intros n H.
(* instead of defining another useless lemma *)
assert (aux: (S n) = (S (add o n))).
simpl. trivial.
(* end of aux *)

rewrite aux in H.
rewrite <- add_branch2 in H.
apply add_elim in H.
(* now H is clearly false. How to ask coq to apply the implied S o != o ? 
inversion seems the closest...
*)
inversion H.
Save.
(* todo find out how to tell coq that sn_never_n is 
the same as Sn <> n by intuitionistic definition of not *)

(* o is the unique neutral element. Note that this is a weak statement. 
  /\ could be an \/ proving that there are no left or right zeros. *)
Theorem o_unique_neutr:
forall z a: nat, (add z a) = a /\ (add a z) = a -> z = o.
Proof.
intros z a H.
destruct H as [Hl Hr]. (* this is how one splits conjunctions in hypotesis *)
apply add_elim in Hl.
assumption.
Save.

Theorem add_assoc:
forall a b c:nat, (add (add a b) c) = (add a (add b c)).
Proof.
intros a b c.
induction c.
- rewrite _o_commutes_right.
  rewrite _o_commutes_right.
  trivial.
- rewrite <- s_commutes_with_add.
  rewrite add_branch2.
  rewrite IHc.
  rewrite <- add_branch2.
  rewrite s_commutes_with_add.
  rewrite <- add_branch2.
  rewrite s_commutes_with_add.
  trivial.
(* wow this was satisfying, went as on paper. *)
Qed.

(* we have proven intuitionistic that add is a commutative monoid *)
