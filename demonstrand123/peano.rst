Axiomatizarea Peano
===================

Sau numere unare.

**Def** :math:`\mathbb{N}`

.. math ::

    0 \in \mathbb{N}
    
    s : \mathbb{N} \to \mathbb{N}
    
    s(a) = s(b) \vdash a = b

    \forall{a}.  s(a) = 0 \vdash \bot

    P(0),\forall{k}. P(k) \to P(k+1) \vdash \forall n. P(n)

Presupune ca avem deja axiome pentru egalitate gen a = a.
Inductia e second order.
    

**Def** :math:`+`

.. math ::

    add: \mathbb{N}, \mathbb{N} \to \mathbb{N}

    add(0, b) &= b

    add(s(a), b) &= s(a, b)





    
