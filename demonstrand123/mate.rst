O scurta introducere in matematica
----------------------------------

Matematica studiaza patternuri.

Un pattern de studiat e ca un proiect in programare.
La inceput se fac experimente, se propun unelte si se lucra la variante simple.

Apoi se construiest librarii de tooluri folositoare la mai multe probleme.
Intr-un final poate proiectul e de succes si patternul de studiat e inteles.
La capat avem un framework cu care putem aborda orice pattern din cele studiate.

Se fac greseli, e la fel de messy ca in programare.
Doar ca Gauss nu a avut jira si a ramas numai frameworkul gata.

Frameworkuri in mate:

    * geometrie: matematica formelor rigide
    * analiza : matematica miscarii
    * algebra : matematica ce generalizeaza numere 
    
Programatorii construiesc cod, matematicieni produc argumente.
Argumentele riguroase is demonstratii.
Demonstratiile pot sa aibe buguri, sa uite sa trateze edge casuri. 

proces
``````

    * conjunctura 
    * definitii ( ce tipuri de date studiem ? )
    * leme (librarii de argumente)
    * teorema ( solutia la problema: argumentul final )
    * teorie (framework pentru toate problemele de un tip)

fundamente
``````````

Fundamentele matematicii au aparut din nevoia de a verifica ca demonstratiile sunt corecte.
Se dorea un sistem de reguli mecanice, formale, universal acceptate. 
Reguli din care sa se poata construi orice argument.

Logica si teoria multimilor e un sistem de acest fel.



