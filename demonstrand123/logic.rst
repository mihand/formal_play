Sistem formal
=============

E un limbaj folosit pentru a scrie argumente riguroase.
Contine:
    
    - niste simboluri
    - gramatica: cum combin simboluri in propozitii valide
    - axiome : propozitii considerate adevarate
    - procedura de decizie: cum creez noi propozitii adevarate

Logica propozitionala
---------------------

Simboluri:

    - atomi: ploua innorat
    - conectori: :math:`\land \lor \lnot` 

Gramatica:
    
    :math:`ploua \land \lnot` nu e o propozitie

    :math:`ploua \lor ninge`  e o propozitie


Procedura de decizie: 

.. math ::

        A, A \to B \vdash B

        A \land B \vdash A, B

        A, B \vdash A \land B

        ... 

Exemplu:

Avem o tabela de fapte adevarate si una de reguli de inferenta.
Axiome sunt faptele adevarate initiale:

.. math ::

    ploua \to innorat

    vara

Ele pot fi simple fapte: e vara sau propozitii mai complicate: daca ploua e innorat.

Adaugam faptul ``ploua`` in tabela de fapte.
Ce noi fapte adevarate se deduc?

.. math ::

    ploua, ploua \to innorat \vdash innorat

    ploua, soare \vdash ploua \land soare

Implicatia :math:`\to` nu e deductie :math:`\vdash` desi sunt intim legate de modulus ponens.

..         
    axiomele nu is deductii, zic numa ce e adevarat
    dar de ex ploua si ploua nu e stiut ca e fapt
    daca e avem urmatoare db de fapte
    ploua si ploua
    si db de reguli
    a si a -> a
    numai dupa procedura de decizie am faptul ca ploua
    hoare and negation
    axiom schema and inductive types:

First order logic
-----------------

Introduce relatii cu argumente propozitii. 

.. math ::

    copil(sabina, maria)
        
    frate(maria, vasile)
    
    frate(M, V) \to frate(V, M)


Dar si cuantificatori.

.. math ::

    frate(M, V) \to \exists P. copil(P, M) \land copil(P, V) 
Demonstratie
------------

O teorema are o impoteza, ceva dat ca adevarat.
Si arata ca e adevarata concluzia.

Ipoteza si concluzia e scrisa in gramatica unei logici.
Pe langa ipoteza mai avem fapte adevarate: axiome luate de bune si alte teoreme demonstrate.

Demonstratia e o succesiune de inferente de la adevaruri spre concluzia teoremei.

.. 
    egalitate si functii ...

